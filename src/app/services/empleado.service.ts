import { Injectable } from '@angular/core';
import { Empleado } from '../models/empleado';

@Injectable({
  providedIn: 'root'
})
export class EmpleadoService {
  listEmpleado: Empleado[] = [
    {
      nombreCompleto:'Lucas Martinez', correo: 'lmartinez@gmail.com', telefono: 1165766767,
      sexo:'Masculino', fechaIngreso: new Date(), estadoCivil: 'Soltero'
    },
    {
      nombreCompleto:'Carlos Gonzalez', correo: 'cgonzalez@gmail.com', telefono: 1167788997,
      sexo:'Masculino', fechaIngreso: new Date(), estadoCivil: 'Casado'
    },
    {
      nombreCompleto:'Marta Sanchez', correo: 'msanchez@hotmail.com', telefono: 1144116767,
      sexo:'Femenino', fechaIngreso: new Date(), estadoCivil: 'Soltero'
    },
    {
      nombreCompleto:'Carla Rodriguez', correo: 'rrodriguez@gmail.com', telefono: 1567873444,
      sexo:'Femenino', fechaIngreso: new Date(), estadoCivil: 'Viuda'
    },
    {
      nombreCompleto:'Lionel Messi', correo: 'lmessi@gmail.com', telefono: 1567221144,
      sexo:'Masculino', fechaIngreso: new Date(), estadoCivil: 'Casado'
    },
    {
      nombreCompleto:'Luis Suarez', correo: 'lsuarez@hotmail.com', telefono: 1567872233,
      sexo:'Masculino', fechaIngreso: new Date(), estadoCivil: 'Casado'
    },
    {
      nombreCompleto:'Cristiano Ronaldo', correo: 'cr7@hotmail.com', telefono: 1589889976,
      sexo:'Masculino', fechaIngreso: new Date(), estadoCivil: 'Casado'
    },
    {
      nombreCompleto:'Gareth Bale', correo: 'gbale@hotmail.com', telefono: 1134872233,
      sexo:'Masculino', fechaIngreso: new Date(), estadoCivil: 'Casado'
    }
];
  constructor() { }

  getEmpleados(){
    return this.listEmpleado.slice();
  }
  eliminarEmpleado(index: number){
    this.listEmpleado.splice(index, 1);
  }
  agregarEmpleado(empleado : Empleado){ 
    this.listEmpleado.unshift(empleado);
  }
  getEmpleado(index: number){
    return this.listEmpleado[index];
  }
  editEmpleado(empleado: Empleado, idEmpleado: number){
    this.listEmpleado[idEmpleado].nombreCompleto = empleado.nombreCompleto;
    this.listEmpleado[idEmpleado].correo = empleado.correo;
    this.listEmpleado[idEmpleado].fechaIngreso = empleado.fechaIngreso;
    this.listEmpleado[idEmpleado].telefono = empleado.telefono;
    this.listEmpleado[idEmpleado].estadoCivil = empleado.estadoCivil;
    this.listEmpleado[idEmpleado].sexo = empleado.sexo;
  }
}
